package com.Module9Java;
import java.math.BigInteger;


public class Operator {
	

	
	
	public double add(BigInteger a, BigInteger b) {
		System.out.println("This is add method for 2 params");
		BigInteger sumResult=a.add(b);
		double sumLog = Math.log(sumResult.doubleValue());
		return sumLog;
	}
	
	public double add(BigInteger a, BigInteger b, BigInteger c) {
		System.out.println("This is add method for 3 params");
		BigInteger sumResult=a.add(b).add(c);
		double sumLog3 = Math.log(sumResult.doubleValue());
		return sumLog3;
	}
	
	public double add(BigInteger a, BigInteger b, BigInteger c, BigInteger d) {
		System.out.println("This is add method for 4 params");
		BigInteger sumResult= a.add(b).add(c).add(d);
		double sumLog4 = Math.log(sumResult.doubleValue());
		return sumLog4;
	}
	public double sub(BigInteger a, BigInteger b) {
		BigInteger subResult= a.subtract(b);
		double subLog = Math.log(subResult.doubleValue());
		return subLog;
	}
	
	public double div(BigInteger a, BigInteger b) {
		BigInteger divResult= a.divide(b);
		double divLog = Math.log(divResult.doubleValue());
		return divLog;
	}
	
	public double mult(BigInteger a, BigInteger b) {
		BigInteger multiResult= a.multiply(b);
		double multiLog = Math.log(multiResult.doubleValue());
		return multiLog;
	}
	
	public double sqrt(int a) {
		return Math.sqrt(a);

		
	public static void main(String[] args){
		Operator operator = new Operator();
		System.out.println(operator.add(new BigInteger("35"),new BigInteger("48")));
		System.out.println(operator.sub(new BigInteger("4403"), new BigInteger("430")));

	}

}


